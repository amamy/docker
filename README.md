# 42 project - Docker

This project aims to help you discovers Docker.

While the subject is outdated, which may leed to bad practices, it has been a great opportunity to start with Docker.
It allowed me to get the basics and from there, I went for tools nowadays used.
<br>
**Questions are mostly about Running containers, link them and finaly build them.**

Following,  the basic questions part of the subject with my asnwers and some explanation.

For each exercise, we will ask you to give the shell command(s) to:
##### 1. Create a virtual machine with docker-machine using the virtualbox driver, and named Char.
```
# Usage : docker-machine [OPTIONS] COMMAND [arg...]
# --driver : specify the driver 
# We can check by doing : docker-machine ls

docker-machine create --driver virtualbox Char
```
##### 2. Get the IP address of the Char virtual machine.
```
# Will aslo work :  docker-machine env Char
# or : docker-machine config Char

docker-machine ls | grep Char
```
##### 3. Define the variables needed by your virtual machine Char in the general env of your terminal, so that you can run the docker ps command without errors. You have to fix all four environment variables with one command, and you are not allowed to use your shell’s builtin to set these variables by hand.
```
# The question is unclear, I do not have any errors doing docker ps... 
# In this question, they tried to ask us to set the env to set the vm as a host. 
# "docker-machine env Char" will display the environment of the VM "Char"
# eval is a built-in, which as I understand, will concatenate the four lines and execute them.
# The VM works well without, it is most needed by future containers if docker is run on Windows or Mac.

eval $(docker-machine env Char)
```
##### 4. Get the hello-world container from the Docker Hub, where it’s available.
```
# Question does not make sense, a container is a running instance of an image.
# The Docker Hub does not contains containers, it contains images.
# To fetch this image without running it : 

docker pull hello-world

```
##### 5. Launch the hello-world container, and make sure that it prints its welcome message, then leaves it.
```
# the image just have to be run, it will exit on his own. To check this : docker container ls --all
docker run hello-world
```
##### 6. Launch an nginx container, available on Docker Hub, as a background task. It should be named overlord, be able to restart on its own, and have its 80 port attached to the 5000 port of Char. You can check that your container functions properly by visiting http://<ip-de-char>:5000 on your web browser.
```
# Launch container in back ground : [OPTION] "-d" (d for Detach)
# Be able to restart : "--restart always" : https://docs.docker.com/config/containers/start-containers-automatically/
# Port binding to Char : https://docs.docker.com/config/containers/container-networking/

docker run --name overlord --restart always -d -p 5000:80 nginx
```
##### 7. Get the internal IP address of the overlord container without starting its shell and in one command.
```
# Docker inspect returns some informations about docker objects.
# Using -f allows us to use GO Template to get only the data we are interested in : the IP address.

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' overlord
```
##### 8. Launch a shell from an alpine container, and make sure that you can interact directly with the container via your terminal, and that the container deletes itself once the shell’s execution is done.
```
# Docker run documentation : https://docs.docker.com/engine/reference/commandline/run/
# -i is for interactive mode, which allows to have a bash access to the container
# --rm : the container will remove itself when exited.

docker run -i --rm alpine
```
##### 9. From the shell of a debian container, install via the container’s package manager everything you need to compile C source code and push it onto a git repo (of course, make sure before that the package manager and the packages already in the container are updated). For this exercise, you should only specify the commands to be run directly in the container.
```
# The last sentence of the question implies that we do not uses docker exec

apt update
apt upgrade
apt install build-essential
apt install git
```
##### 10. Create a volume named hatchery.
```
# The command, here, do exactly what it says : create a docker volume named hatchery
 docker volume create hatchery
```
##### 11. List all the Docker volumes created on the machine. Remember. VOLUMES.
```
docker volume ls
```
##### 12. Launch a mysql container as a background task. It should be able to restart on its own in case of error, and the root password of the database should be Kerrigan. You will also make sure that the database is stored in the hatchery volume, that the container directly creates a database named zerglings, and that the container itself is named spawning-pool.
```
# [OPTION] : -d -> to run in background.
# [OPTION] : --restart on-failure -> If the container stops because of an error, it will restart.
# [OPTION] : --mount source=hatchery,target=target=/var/lib/mysql -> mount the docker volume named hatchery to the location /var/lib/mysql of the container.
# [IMAGE]  : mysql:5.7 : We specify this version now because the subject is a bit old and in question 14, we are asked to do something which is not popular anymore
# and latest mysql version has an authentication issue with docker.

docker run \
-d \
--name spawning-pool \
--restart on-failure \
--mount source=hatchery,target=/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=Kerrigan \
-e MYSQL_DATABASE=zerglings \
mysql:5.7
```
##### 13. Print the environment variables of the spawning-pool container in one command, to be sure that you have configured your container properly.
```
# docker exec is a command which allows to execute a command in a container. 
# docker exec usage : [CONTAINER] [COMMAND_TO_EXECUTE]

docker exec spawning-pool env
```
##### 14. Launch a wordpress container as a background task, just for fun. The container should be named lair, its 80 port should be bound to the 8080 port of the virtual machine, and it should be able to use the spawning-pool container as a database service. You can try to access lair on your machine via a web browser, with the IP address of the virtual machine as a URL. Congratulations, you just deployed a functional Wordpress website in two commands!
```
# This question has been written years ago, and, unfortunately ask to use '--link' which the maintainer of docker chose to remove in the future.
# The recommended method now is to use the networks, solution commented and explained bellow.
#
# Explaination of subject way : 
#
# -p 8080:80 : bind port 80 to Char:8080
# --link spawning-pool : makes a network link between the mysql container to the wordpres container
# -e : Exports of usefull env var to connect DB.
#
# Recommanded way : 
# The --link way as flaws while inter connect several machines, think about the number of links for machines, you have to link every machine to every other in a infrastructure.
# docker network implement networks for containers and allow us to put as many containers as we want in a network and make them communicate together : 
#
#	-Network creation : 
# docker network create webnet 
# 
#	Run Mysql container IN the "webnet" network
# docker run \
# -d \
# --name spawning-pool \
# --net=webnet \
# --restart on-failure \
# --mount source=hatchery,target=/var/lib/mysql \
# -e MYSQL_ROOT_PASSWORD=Kerrigan \
# -e MYSQL_DATABASE=zerglings \
# mysql:5.7
# 
# 	Run Wordpress container in "webnet" network
# docker run \
# -d \
# --name lair \
# --net=webnet \
# -p 8080:80 \
# -e WORDPRESS_DB_HOST=spawning-pool \
# -e WORDPRESS_DB_NAME=zerglings \
# -e WORDPRESS_DB_USER=root \
# -e WORDPRESS_DB_PASSWORD=Kerrigan \
# wordpress
# 
#
# Note : in short, we replace the line '--link' in the wordpress container by the network link.
#
# TO TEST :
# get <lair-ip> : docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' lair
# http://<lair-ip>:8080

docker run \
-d \
--name=lair \
-p 8080:80 \
--link spawning-pool \
-e WORDPRESS_DB_HOST=spawning-pool \
-e WORDPRESS_DB_USER=root \
-e WORDPRESS_DB_PASSWORD=Kerrigan \
-e WORDPRESS_DB_NAME=zerglings \
wordpress
```
##### 15. Launch a phpmyadmin container as a background task. It should be named roach-warden, its 80 port should be bound to the 8081 port of the virtual machine and it should be able to explore the database stored in the spawning-pool container.
```
# -e PMA_HOST=spawning-pool : Send var env used by phpmyadmin to connect to the DB.
#
# Recomended way : 
# docker run \
# -d \
# --name roach-warden \
# -p 8081:80 \
# --net=webnet \
# -e PMA_HOST=spawning-pool \
# phpmyadmin/phpmyadmin
#
# TO TEST : 
# get <roach-warden-ip> : docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' roach-warden
# http://<roach-warden-ip>:8081

docker run \
-d \
--name roach-warden \
-p 8081:80 \
--link spawning-pool \
-e PMA_HOST=spawning-pool \
phpmyadmin/phpmyadmin
```
##### 16. Look up the spawning-pool container’s logs in real time without running its shell.
```
# docker logs [CONTAINER] -> give the logs of the specified container.
# --follow : Follow log output
# --timestamps : Show timestamps
	
docker logs --follow --timestamps lair
```
##### 17. Display all the currently active containers on the Char virtual machine.
```
docker ps
```
##### 18. Relaunch the overlord container.
```
docker container restart overlord
```
##### 19. Launch a container name Abathur. It will be a Python container, 2-slim version, its /root folder will be bound to a HOME folder on your host, and its 3000 port will be bound to the 3000 port of your virtual machine.
You will personalize this container so that you can use the Flask micro-framework in its latest version. You will make sure that an html page displaying "Hello World" with \<h1> tags can be served by Flask. You will test that your container is properly set up by accessing, via curl or a web browser, the IP address of your virtual machine on the 3000 port. You will also list all the necessary commands in your repository.
```
# --mount type=bind : mount of type bind are used to mount a file from the hostś filesystem
# All docker exec are for configuring flask, not really related to docker.

docker run \
-dit \
--name Abathur \
--mount type=bind,source=/home/docker,target=/root \
-p 3000:3000 \
python:2-slim

docker exec Abathur pip install flask

docker exec Abathur bash -c "echo 'from flask import Flask' >> /root/hello_world.py"
docker exec Abathur bash -c "echo 'app = Flask(__name__)' >> /root/hello_world.py"
docker exec Abathur bash -c "echo \"@app.route('/')\" >> /root/hello_world.py"
docker exec Abathur bash -c "echo 'def hello_world():' >> /root/hello_world.py"
docker exec Abathur bash -c "echo -e \"\treturn '<h1>Hello World</h1>'	\" >> /root/hello_world.py"

docker exec Abathur bash -c "export FLASK_APP=/root/hello_world.py && flask run -h 0.0.0.0 -p 3000"
```
##### 20. Create a local swarm, the Char virtual machine should be its manager.
```
# 1) Get the future manager node env to senf docker swarm init its infos.
# 2) Make that node a swarm manager
# 3) Save the wroker token in a worker_token file, The commad stored in it will allow us to add workers later.
# --advertise-addr : the manager node publish an ip address which is not mandatory to be his own. It is here.

eval $(docker-machine env Char)
docker swarm init --advertise-addr $(docker-machine ip Char)
docker swarm join-token worker -q > ~/.worker_token
```
##### 21. Create another virtual machine with docker-machine using the virtualbox driver, and name it Aiur.
```
docker-machine create --driver virtualbox Aiur
```
##### 22. Turn Aiur into a slave of the local swarm in which Char is leader (the command to take control of Aiur is not requested).
```
# 1) Get the future worker node env to senf docker swarm init its infos.
# This token is created while swarm init, it allows us to identify the swarm.
#
# To test : docker infos

eval $(docker-machine env Aiur)
docker swarm join --token $(cat ~/.worker_token) $(docker-machine ip Char):2377

```
##### 23. Create an overlay-type internal network that you will name overmind.
```
# -d overlay: specify that we want an overlay driver type instead of default bridge.

docker network create -d overlay overmind
```
##### 24. Launch a rabbitmq SERVICE that will be named orbital-command. You should define a specific user and password for the RabbitMQ service, they can be whatever you want. This service will be on the overmind network.
```
# Docker services are used when running multiple containers. 
# This question makes a service looks like a container but it is not.

docker service create \
--name orbital-command \
--network=overmind \
-e RABBITMQ_DEFAULT_USER=user \
-e RABBITMQ_DEFAULT_PASS=password \
rabbitmq
```
##### 25. List all the services of the local swarm.
```
docker service ls
```
##### 26. Launch a 42school/engineering-bay service in two replicas and make sure that the service works roperly (see the documentation provided at hub.docker.com). This service will be named engineering-bay and will be on the overmind network.
```
# To test : docker service ps engineering-bay

docker service create \
--name engineering-bay \
--replicas 2 \
--network=overmind \
-e OC_USERNAME=user \
-e OC_PASSWD=password \
42school/engineering-bay
```
##### 27. Get the real-time logs of one the tasks of the engineering-bay service.
```
docker service logs engineering-bay
```
##### 28. ... Damn it, a group of zergs is attacking orbital-command, and shutting down the engineering-bay service won’t help at all... You must send a troup of Marines to eliminate the intruders. Launch a 42school/marine-squad in two replicas, and make sure that the service works properly (see the documentation provided at hub.docker.com). This service will be named... marines and will be on the overmind network.
```
docker service create \
--name marines \
--replicas 2 \
--network overmind \
-e OC_USERNAME=user \
-e OC_PASSWD=password \
42school/marine-squad
```
##### 29. Display all the tasks of the marines service.
```
docker service ps marines
```
##### 30. Increase the number of copies of the marines service up to twenty, because there’s never enough Marines to eliminate Zergs. (Remember to take a look at the tasks and logs of the service, you’ll see, it’s fun.)
```
docker service scale marines=20
```
##### 31. Force quit and delete all the services on the local swarm, in one command.
```
# docker service ls : list services
# -q : displays only IDs.

docker service rm $(docker service ls -q)
```
##### 32. Force quit and delete all the containers (whatever their status), in one command.
```
# docker rm -f : force remove containers args
# -a : list all containers
# -q : displays only IDs.

docker rm -f $(docker ps -a -q)
```
##### 33. Delete all the container images stored on the Char virtual machine, in one command as well.
```
# docker rmi : removes images

docker rmi -f $(docker images -q)
```
##### 34. Delete the Aiur virtual machine without using rm -rf.
```
docker-machine rm -y Aiur
```